package restassured_referance;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import io.restassured.RestAssured;

public class Get_Gorest {

	public static void main(String[] args) {

		// Step 1 - Declare Base URL
		RestAssured.baseURI = "https://gorest.co.in/";

		// Step 2 - Validate status code
		int status_code = given().when().get("public/v2/users").then().extract().response().statusCode();
		System.out.println(status_code);
		Assert.assertEquals(status_code, 200);

		// Step 3 - Configure the response body and Trigger the API
		String responsebody = given().when().get("public/v2/users").then().extract().response().asString();
		System.out.println("response body :" + responsebody);
		
		// Step 4 - Declare the Expected result
		int exp_id[] = { 5710619, 5710618, 5710614, 5710613, 5710612, 5710611, 5710610, 5710609, 5710607, 5710606 };
		

		// Step 5 - Create JSON Object path for parse the response body
		JSONObject res_array = new JSONObject(responsebody);
		JSONArray  data_array = res_array.getJSONArray("data");
		System.out.println(data_array);
		int count = data_array.length();
		System.out.println(count);
	}

}
