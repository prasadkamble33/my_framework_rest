package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class Post_Referance_1 {

	public static void main(String[] args) {
		//Step 1 - Declare the Base URL
	    RestAssured.baseURI = "https://reqres.in/";
	    
	    // Step 2 - Validate the Status code
	    int statuscode = given().header("Content-Type","application/json").when().post("api/register").then().extract().response().statusCode();
	    System.out.println("statuscode:"+statuscode);
	    Assert.assertEquals(statuscode, 400);
	    
	    // Step 3 - Configure the request parameter and trigger the API
	    String requestbody = "{\r\n"
	    		+ "    \"email\": \"eve.holt@reqres.in\",\r\n"
	    		+ "    \"password\": \"pistol\"\r\n"
	    		+ "}";
	    
	    String responsebody = given().header("Content-Type","application/json").body("{\r\n"
	    		+ "    \"email\": \"eve.holt@reqres.in\",\r\n"
	    		+ "    \"password\": \"pistol\"\r\n"
	    		+ "}").when().post("api/register").then().extract().response().asString();
	    
	    //Step 4 - Create an Object of Json Path for parse the request body and response body
	    JsonPath jsp_req = new JsonPath(requestbody);
	    System.out.println("requestbody:"+jsp_req);
	    
	    String req_email = jsp_req.getString("eve.holt@reqres.in");
	    System.out.println("email:"+req_email);
	    
	    JsonPath jsp_res = new JsonPath(responsebody);
	    System.out.println("responsebody:"+jsp_res);
	    
	    String res_id = jsp_res.getString("4");
	   // System.out.println("id:"+res_id);
	    
	    String res_token = jsp_res.getString("QpwL5tke4Pnpja7X4");
	    System.out.println("token:"+res_token);
	    
	    // Step 5 - Validate the response body
	   // Assert.assertEquals(res_id,"4");
	    //Assert.assertEquals(res_token,"QpwL5tke4Pnpja7X4");
	    
	    

	}

}
