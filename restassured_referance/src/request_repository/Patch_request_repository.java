package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_method.Excel_data_exctracor_2;

public class Patch_request_repository {

	public static String patch_request_TC3() throws IOException {
		ArrayList<String> Data = Excel_data_exctracor_2.Excel_Data_Reader("Test_Data","Patch_API","Patch_TC3");
		
		String name = Data.get(1);
		String job  = Data.get(2);
		//String patch_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"+ "}";
		String patch_requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n"+ "}";
		return patch_requestBody;
	}

}
