package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_method.Excel_data_exctracor_2;

public class Put_request_repository {

	public static String put_request_TC2() throws IOException {
		ArrayList<String> Data = Excel_data_exctracor_2.Excel_Data_Reader("Test_Data", "Put_API", "Put_TC4");
		String name = Data.get(1);
		String job = Data.get(2);
		//String put_requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String put_requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n"
				+ "}";
		return put_requestbody;

	}

}