package utility_common_method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Extractor {

	public static void main(String[] args) throws IOException {
		String project_dir = System.getProperty("user.dir");
		// System.out.println(project_dir);

		// Step 1-: Create the object of file input stream to locate the data file.
		FileInputStream FIS = new FileInputStream(project_dir + "\\Data_File\\Test_Data.xlsx");

		// Step 2-: Create the XSSFWorkbook object to open the excel file.
		XSSFWorkbook WorkBook = new XSSFWorkbook(FIS);

		// Step 3-: Fetch the number of sheets available in the excel file.
		int count = WorkBook.getNumberOfSheets();
		// System.out.println(count);

		// Step 4-: Access the sheet as per the Input Sheet name.
		for (int i = 0; i < count; i++) {
			String SheetName = WorkBook.getSheetName(i);
			// System.out.println(SheetName);

			if (SheetName.equals("Put_API")) {
				//System.out.println(SheetName);
				XSSFSheet Sheet = WorkBook.getSheetAt(i);
				Iterator<Row> row = Sheet.iterator();
				row.next();
				
				while (row.hasNext()) {
					Row Data_Row = row.next();
					String TC_Name = Data_Row.getCell(0).getStringCellValue();
					//System.out.println(TC_Name);
					
					if (TC_Name.equals("Put_TC3")) {
						Iterator<Cell> Cell_Values = Data_Row.iterator();
						while (Cell_Values.hasNext()) {
							String Test_Data = Cell_Values.next().getStringCellValue();
							System.out.println(Test_Data);
						}
						//break;
					}
				}
				break;
			}
		}
	}
}
		
		
			

