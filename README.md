# My_Rest_Assured_FrameWork


#	My Framework combination of data driven frame work and keyword Driven Framework.

	My Framework consist of six components are: -
1.	Test Driver mechanism.
2.	Test Script package
3.	Common Functions 
    3.1.API related common functions
    3.2.Utilities related common functions
4.	Data file
5.	Libraries 
6.	Report mechanism.

In my current framework to drive my test execution using Static and Dynamic Driver class.

In Static driver class, I have imported all test cases of test script package by calling respective method statically and executed them. But as name suggest Static driver class are Static in nature so I have to write script name clearly in test driver mechanism.

In Dynamic driver class by using java.lang.reflect, I have called the test class dynamically at runtime from my test script package and executed them.

In test script package I have all test case with specific test class and I have multiple test cases for each API and for each test case I have one single test class.

In common functions divided into two categories: -
1.API related common functions: -
     This is used for trigger API and fetch the status code and response body.
2. Utility related common functions: -
     1st Utility related common function is use to create directory after executing my test cases directory created automatically if it’s does not exist and if it’s does exist then deleted and created again.
      2nd Utility related common function is use to create API logs into text file that mean API executed corresponding endpoint, request body and response body log have been generated and save into this log files.
      3rd Utility related common function is use to read data from excel file.

I can read data from excel file by using Apache poi libraries. I created utility for same. This completes common functions component.

In libraries which are using into my framework are rest assured to trigger API and extract response status code and extract response body.

By using Json path I can parse response body and request body for rest API and used xml path for SOAP API and then using testNG libraries we are validating response body parameter with help of  Assert class. 

I have used Apache POI libraries for reading data from Excel file and also used Extents and Allure libraries for reports generations.  

Then I have reporting mechanism, I using extents report currently. Early using Allure report but due to caches related issue start using extent report.  
